import Vue from 'vue';

import App from './components/app.vue';

function isMobile() {
	const w = window;
	const d = document;
	const e = d.documentElement;
	const g = d.getElementsByTagName('body')[0];
	const x = w.innerWidth || e.clientWidth || g.clientWidth;
	const y = w.innerHeight || e.clientHeight || g.clientHeight;
	return x < y * 0.75;
}

const app = new Vue({
	el: '#app',
	render: h => h(App),
});

try {
	if (PRODUCTION && 'serviceWorker' in navigator) {
		window.addEventListener('load', () => {
			navigator.serviceWorker.register('./service-worker.js');
		});
	}
} catch (e) {
	console.log('dev mode, no service worker');
}
